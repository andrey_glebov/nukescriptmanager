/*
 * Copyright (c) 2015. Andrey Glebov <andrey458641387@gmail.com>.
 * All rights reserved.
 */

package com.tiav.nukerendermanager;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

/**
 * {@code ProgressDialogController} is the JavaFX controller for the {@code ProgressDialog} of NukeRenderManager
 *
 * @author Andrey Glebov
 * @version 0.0.2
 * @since 0.0.2
 */
public class ProgressDialogController {

    @FXML
    private Label processTypeName;
    @FXML
    private Label subtaskNameLabel;
    @FXML
    private Label subtaskProgressLabel;
    @FXML
    private ProgressBar subtaskProgressBar;
    @FXML
    private Label taskNameLabel;
    @FXML
    private Label taskProgressLabel;
    @FXML
    private ProgressBar taskProgressBar;
    @FXML
    private Label overallProcessLabel;
    @FXML
    private Label overallProgressLabel;
    @FXML
    private ProgressBar overallProgressBar;

    void setProcessType(String text) {
        processTypeName.setText(text);
    }

    void setSubtaskNameLabel(String text) {
        subtaskNameLabel.setText(text);
    }

    void setSubtaskProgressLabel(String text) {
        subtaskProgressLabel.setText(text);
    }

    void setSubtaskProgressBar(double value) {
        subtaskProgressBar.setProgress(value);
    }

    void setTaskNameLabel(String text) {
        taskNameLabel.setText(text);
    }

    void setTaskProgressLabel(String text) {
        taskProgressLabel.setText(text);
    }

    void setTaskProgressBar(double value) {
        taskProgressBar.setProgress(value);
    }

    void setOverallProcessLabel(String text) {
        overallProcessLabel.setText(text);
    }

    void setOverallProgressLabel(String text) {
        overallProgressLabel.setText(text);
    }

    void setOverallProgressBar(double value) {
        overallProgressBar.setProgress(value);
    }

}
