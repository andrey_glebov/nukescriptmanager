/*
 * Copyright (c) 2015. Andrey Glebov <andrey458641387@gmail.com>.
 * All rights reserved.
 */

package com.tiav.nukerendermanager;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * ScriptEntry represents an entry in a list of Nuke(R) scripts
 * Acts as a container for data about a script
 *
 * @author Andrey Glebov
 * @version 0.0.2
 * @since 0.0.1
 */
class ScriptEntry {

    /**
     * The script's ID
     */
    private IntegerProperty scriptId;
    /**
     * Path to the script file
     *
     * @since 0.0.2
     */
    private StringProperty scriptPath;

    /**
     * Default constructor of {@code ScriptEntry}
     *
     * @since 0.0.1
     */
    ScriptEntry() {
        scriptId = new SimpleIntegerProperty();
        scriptPath = new SimpleStringProperty();
    }

    /**
     * Initializing constructor of {@code ScriptEntry}
     *
     * @param id   the script's ID
     * @param path path to the script file
     * @since 0.0.1
     */
    ScriptEntry(Integer id, String path) {
        scriptId = new SimpleIntegerProperty(id);
        scriptPath = new SimpleStringProperty(path);
    }

    /**
     * Returns the script's ID as {@link javafx.beans.property.IntegerProperty}
     *
     * @return the script's ID Property
     * @since 0.0.1
     */
    IntegerProperty scriptIdProperty() {
        return scriptId;
    }

    /**
     * Returns the path to the script file as {@link javafx.beans.property.StringProperty}
     *
     * @return the path to the script file
     * @since 0.0.1
     */
    StringProperty scriptPathProperty() {
        return scriptPath;
    }

    /**
     * Returns the script's ID as an {@code Integer}
     *
     * @return the script's ID
     * @since 0.0.1
     */
    Integer getScriptId() {
        return scriptId.get();
    }

    /**
     * Sets the script's ID
     *
     * @param id the script's ID
     * @since 0.0.1
     */
    void setScriptId(Integer id) {
        scriptId.set(id);
    }

    /**
     * Returns the path to the script as {@code String}
     *
     * @return path to the script
     * @since 0.0.1
     */
    String getScriptPath() {
        return scriptPath.get();
    }

    /**
     * Sets the path to the script file
     *
     * @param name path to the script file
     * @since 0.0.1
     */
    void setScriptPath(String name) {
        scriptPath.set(name);
    }

}
