/*
 * Copyright (c) 2015. Andrey Glebov <andrey458641387@gmail.com>.
 * All rights reserved.
 */

package com.tiav.nukerendermanager;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import java.util.regex.PatternSyntaxException;

/**
 * MainWindowController is the JavaFX controller for the main window of NukeRenderManager
 *
 * @author Andrey Glebov
 * @version 0.0.2
 * @since 0.0.1
 */
public class MainWindowController {

    /**
     * Chooses if Write nodes are filtered by name
     */
    @FXML
    private ToggleButton renderNodesNamedToggle;
    /**
     * Hods names of Write nodes to be rendered
     */
    @FXML
    private TextField renderWriteNodeNamesText;
    /**
     * A table that represents the render queue
     */
    @FXML
    private TableView<ScriptEntry> renderScriptTable;
    /**
     * The ID column in the {@code renderScriptTable}
     */
    @FXML
    private TableColumn<ScriptEntry, Integer> scriptIdColumn;
    /**
     * The Name column in the {@code renderScriptTable}
     */
    @FXML
    private TableColumn<ScriptEntry, String> scriptNameColumn;

    /**
     * References the main NukeRenderManager instance
     */
    private NukeRenderManager renderManager;


    /**
     * Constructor of {@code MainWindowController}
     * Is called before the FXML file is loaded
     *
     * @since 0.0.1
     */
    public MainWindowController() {
    }

    /**
     * Is called after the FXML file is loaded
     * Binds {@code CellValueFactory}s to {@code renderScriptTable}'s columns
     *
     * @since 0.0.1
     */
    @FXML
    private void initialize() {
        scriptIdColumn.setCellValueFactory(cellData -> cellData.getValue().scriptIdProperty().asObject());
        scriptNameColumn.setCellValueFactory(cellData -> cellData.getValue().scriptPathProperty());
    }

    /**
     * Saves a reference to the main NukeRenderManager instance to {@code renderManager}
     * Binds data to {@code renderScriptTable}
     *
     * @param renderManager the main NukeRenderManager instance
     * @since 0.0.1
     */
    void setNukeRenderManager(NukeRenderManager renderManager) {
        this.renderManager = renderManager;
        renderScriptTable.setItems(renderManager.getScriptRenderList());
    }

    /**
     * Updates the list of scripts to render after a rebuild
     *
     * @since 0.0.2
     */
    void updateRenderScriptTableData() {
        renderScriptTable.setItems(renderManager.getScriptRenderList());
    }

    /**
     * Handles an {@code ActionEvent} on the "Add scripts..." button of the "Render" page
     *
     * @param actionEvent ActionEvent to handle
     * @since 0.0.1
     */
    @FXML
    private void handleRenderAddScriptsButtonAction(ActionEvent actionEvent) {
        renderManager.renderAddScripts();
    }

    /**
     * Handles an {@code ActionEvent} on the "Add folder..." button of the "Render" page
     *
     * @param actionEvent ActionEvent to handle
     * @since 0.0.1
     */
    @FXML
    private void handleRenderAddDirButtonAction(ActionEvent actionEvent) {
        renderManager.renderAddDirectory();
    }

    /**
     * Handles an {@code ActionEvent} on the "Write only nodes named..." {@code ToggleButton} of the "Render" page
     * Toggles the {@code renderWriteNodeNamesText} field's accessibility
     *
     * @param actionEvent ActionEvent to handle
     * @since 0.0.1
     */
    @FXML
    private void handleRenderNodesNamedToggleAction(ActionEvent actionEvent) {
        if (renderNodesNamedToggle.isSelected()) {
            renderWriteNodeNamesText.setEditable(true);
            renderWriteNodeNamesText.setDisable(false);
        } else {
            renderWriteNodeNamesText.setEditable(false);
            renderWriteNodeNamesText.setDisable(true);
        }
    }

    /**
     * Handles a key press in the renderScriptsTable
     * Deletes selected script
     *
     * @param keyEvent KeyEvent to handle
     * @since 0.0.1
     */
    @FXML
    private void handleScriptTableKeyReleased(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.DELETE) {
            int selectedIndex = renderScriptTable.getSelectionModel().getSelectedIndex();
            if (selectedIndex >= 0) {
                Action response = Dialogs.create()
                        .owner(renderManager.getPrimaryStage())
                        .title("Remove script from render queue")
                        .message("Do you really want to remove this script?")
                        .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                        .showConfirm();
                if (response == Dialog.ACTION_YES) {
                    try {
                        renderScriptTable.getItems().remove(selectedIndex);
                    } catch (IndexOutOfBoundsException exception) {
                        // Cannot be thrown as selectedIndex is checked
                        exception.printStackTrace();
                    }
                    renderManager.rebuildRenderScriptList();
                }
            }
        }
    }

    /**
     * Handles an {@code ActionEvent} on the "Render" button of the "Render" page
     * Starts the render queue
     *
     * @param actionEvent ActionEvent to handle
     * @since 0.0.1
     */
    @FXML
    private void handleRenderScriptsButtonAction(ActionEvent actionEvent) {
        Action response = Dialogs.create()
                .owner(renderManager.getPrimaryStage())
                .title("Start rendering queue")
                .message("Do you want to really start rendering?")
                .actions(Dialog.ACTION_YES, Dialog.ACTION_NO)
                .showConfirm();
        if (response == Dialog.ACTION_YES) {
            if (renderNodesNamedToggle.isSelected()) {
                String[] writeNodeNames = null;
                try {
                    writeNodeNames = renderWriteNodeNamesText.getText().split("\\s+");
                } catch (PatternSyntaxException exception) {
                    // Cannot be thrown as the regex is hardcoded and valid
                    exception.printStackTrace();
                }
                renderManager.renderScripts(true, writeNodeNames);
            } else {
                renderManager.renderScripts(false, null);
            }
        }
    }

}
