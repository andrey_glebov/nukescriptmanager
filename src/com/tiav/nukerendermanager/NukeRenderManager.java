/*
 * Copyright (c) 2015. Andrey Glebov <andrey458641387@gmail.com>.
 * All rights reserved.
 */

package com.tiav.nukerendermanager;

import com.tiav.net.TcpMessageSocket;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

/**
 * NukeRenderManager is the main class for the NukeRenderManager application
 * It handles setting up the GUI, slave node and controlling all tasks
 *
 * @author Andrey Glebov
 * @version 0.0.2
 * @since 0.0.1
 */
public class NukeRenderManager extends Application {

    private String nukeExePath = "\"C:\\Program Files\\Nuke9.0v3\\Nuke9.0.exe\"";
    private String ffmpegExePath = "C:\\ffmpeg\\bin\\ffmpeg.exe";
    private String slaveNodePath = "D:\\dev\\NukeRenderManager\\python\\NukeSlaveNode.py";

    private ServerSocket server = null;
    private Process slaveNodeProcess = null;
    private TcpMessageSocket slaveNodeSocket = null;
    private boolean slaveNodeReady = false;

    private Stage primaryStage = null;
    private MainWindowController controller = null;

    private ObservableList<ScriptEntry> scriptRenderList = FXCollections.observableArrayList();
    private int scriptRenderListLastId = 0;

    public NukeRenderManager() {
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException exception) {
            System.out.println("Couldn't load the GUI resource");
            exception.printStackTrace();
            primaryStage.close();
            System.exit(1);
        }
        controller = loader.getController();
        controller.setNukeRenderManager(this);

        primaryStage.setTitle("Nuke Render Manager");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

    /**
     * Shows a file chooser dialog and ads selected files to the render queue
     *
     * @since 0.0.1
     */
    void renderAddScripts() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Add scripts to render queue");
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Nuke scripts (*.nk)", "*.nk");
        fileChooser.getExtensionFilters().add(filter);

        List<File> fileList = fileChooser.showOpenMultipleDialog(primaryStage);
        if (fileList != null) {
            for (File file : fileList) {
                addRenderScript(file.getAbsolutePath());
            }
        }
    }

    /**
     * Shows a directory chooser dialog and ads script files to the render queue
     *
     * @since 0.0.1
     */
    void renderAddDirectory() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Add scripts in folder to render queue");

        File directory = directoryChooser.showDialog(primaryStage);
        if ((directory != null) && (directory.isDirectory())) {
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".nk");
                }
            };
            File[] fileList = directory.listFiles(filter);
            for (File file : fileList) {
                addRenderScript(file.getAbsolutePath());
            }
        }
    }

    /**
     * Ads a script to the render queue if it is not already in the list
     *
     * @param fileName path to the script file
     * @since 0.0.1
     */
    private void addRenderScript(String fileName) {
        boolean exists = false;
        for (ScriptEntry scriptEntry : scriptRenderList)
            if (fileName.equals(scriptEntry.getScriptPath())) {
                exists = true;
                break;
            }
        if (!exists)
            scriptRenderList.add(new ScriptEntry(++scriptRenderListLastId, fileName));
    }

    /**
     * Updates the script's IDs in the render queue after entries have been deleted
     *
     * @since 0.0.2
     */
    void rebuildRenderScriptList() {
        ObservableList<ScriptEntry> rebuilt = FXCollections.observableArrayList();
        scriptRenderListLastId = 0;
        for (ScriptEntry script : scriptRenderList)
            rebuilt.add(new ScriptEntry(++scriptRenderListLastId, script.getScriptPath()));
        scriptRenderList = rebuilt;
        controller.updateRenderScriptTableData();
    }

    private ProgressDialogController newProgressDialog(String type) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(NukeRenderManager.class.getResource("ProgressDialog.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Rendering...");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            ProgressDialogController progressController = loader.getController();
            progressController.setProcessType("Rendering...");

            dialogStage.show();
            return progressController;

        } catch (IOException e) {
            System.out.println("Couldn't open progress dialog window");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Renders the render script list
     *
     * @param renderOnlyNodesNamed Whether to only render nodes with names, specified by {@code writeNodeNames}
     * @param writeNodeNames       Array of names of Write nodes to render,
     *                             can be {@code null} if {@code renderOnlyNodesNamed} is {@code false}
     * @since 0.0.1
     */
    void renderScripts(final boolean renderOnlyNodesNamed, final String[] writeNodeNames) {
        final ProgressDialogController progressController = newProgressDialog("Render");

        final Task<Boolean> renderTask = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                Platform.runLater(() -> {
                    progressController.setOverallProcessLabel("Setting up internal server...");
                });
                if (!setUpServer()) {
                    Platform.runLater(() -> {
                        progressController.setOverallProcessLabel("Internal server failed");
                    });
                    return Boolean.FALSE;
                }
                Platform.runLater(() -> {
                    progressController.setOverallProcessLabel("Setting up nuke slave node...");
                });
                if (!setUpSlave()) {
                    Platform.runLater(() -> {
                        progressController.setOverallProcessLabel("Nuke slave node failed");
                    });
                    return Boolean.FALSE;
                }

                final int scriptCount = scriptRenderList.size();
                int currentScriptNodeCount = 0;
                int currentScriptFrameCount;
                int currentScriptNumber = 0;
                int currentNodeNumber = 0;
                int currentScriptFirstFrame;
                int currentScriptLastFrame;
                double frameInNode;
                double frameInScript;
                double nodeInScript;
                double frameInAll;
                double nodeInAll;
                final double scriptInAll = 1.0 / (double) scriptCount;

                Platform.runLater(() -> {
                    progressController.setOverallProgressBar(0.0);
                    progressController.setOverallProgressLabel("0/" + scriptCount);
                });
                for (ScriptEntry scriptEntry : scriptRenderList) {
                    JSONObject json = makeRenderJson(scriptEntry.getScriptPath(), renderOnlyNodesNamed, writeNodeNames);
                    try {
                        Platform.runLater(() -> {
                            progressController.setOverallProcessLabel("Loading script...");
                        });
                        slaveNodeSocket.writeJson(json);
                    } catch (IOException exception) {
                        Platform.runLater(() -> {
                            progressController.setOverallProcessLabel("Couldn't send the request to slave node");
                        });
                        System.out.println("Couldn't send the request to slave node");
                        return Boolean.FALSE;
                    }

                    JSONObject scriptRenderData = slaveNodeSocket.readJson();
                    if (scriptRenderData.getString("mode").equals("scriptRenderData")) {
                        currentScriptNodeCount = scriptRenderData.getInt("nodeCount");
                        currentScriptFirstFrame = scriptRenderData.getInt("firstFrame");
                        currentScriptLastFrame = scriptRenderData.getInt("lastFrame");

                        nodeInScript = 1.0 / (double) currentScriptNodeCount;
                        nodeInAll = scriptInAll / (double) currentScriptNodeCount;
                        currentScriptFrameCount = currentScriptLastFrame - currentScriptFirstFrame + 1;
                        frameInNode = 1.0 / (double) currentScriptFrameCount;
                        frameInScript = nodeInScript / (double) currentScriptFrameCount;
                        frameInAll = nodeInAll / (double) currentScriptFrameCount;

                        final int nodeCount = currentScriptNodeCount;
                        final int frameCount = currentScriptFrameCount;
                        Platform.runLater(() -> {
                            progressController.setOverallProcessLabel("Script: " + scriptRenderData.getString("script"));
                            progressController.setTaskProgressLabel("0/" + nodeCount);
                            progressController.setSubtaskProgressLabel("0/" + frameCount);
                        });
                    } else {
                        Platform.runLater(() -> {
                            progressController.setOverallProcessLabel("Error communicating with slave node");
                        });
                        System.out.println("Error communicating with slave node");
                        return Boolean.FALSE;
                    }

                    boolean processing = true;
                    while (processing) {
                        JSONObject slaveFeedback = slaveNodeSocket.readJson();
                        if (slaveFeedback.has("mode")) {
                            switch (slaveFeedback.getString("mode")) {
                                case "nodeRenderData":
                                    Platform.runLater(() -> {
                                        progressController.setTaskNameLabel(slaveFeedback.getString("name") + ": " +
                                                slaveFeedback.getString("file"));
                                        progressController.setTaskProgressBar(0.0);
                                        progressController.setSubtaskProgressBar(0.0);
                                    });
                                    boolean rendering = true;
                                    int currentFrameNumber = currentScriptFirstFrame - 1;
                                    int currentFrameCounter = 0;
                                    while (rendering) {
                                        JSONObject renderFeedback = slaveNodeSocket.readJson();
                                        switch (renderFeedback.getString("mode")) {
                                            case "frameRenderBegin":
                                                if (renderFeedback.getInt("frame") == currentFrameNumber + 1) {
                                                    final int frame = ++currentFrameNumber;
                                                    Platform.runLater(() -> {
                                                        progressController.setSubtaskNameLabel("Frame: " + frame);
                                                    });
                                                } else {
                                                    Platform.runLater(() -> {
                                                        progressController.setSubtaskNameLabel("Wrong render order");
                                                    });
                                                    return Boolean.FALSE;
                                                }
                                                break;
                                            case "frameRenderEnd":
                                                if (renderFeedback.getInt("frame") == currentFrameNumber) {
                                                    final int frame = ++currentFrameCounter;
                                                    final int script = currentScriptNumber;
                                                    final int currentNodeNumberFinal = currentNodeNumber;
                                                    final double frameInNodeFinal = frameInNode;
                                                    final double nodeInScriptFinal = nodeInScript;
                                                    final double frameInScriptFinal = frameInScript;
                                                    final double nodeInAllFinal = nodeInAll;
                                                    final double frameInAllFinal = frameInAll;
                                                    final int frameCount = currentScriptLastFrame;
                                                    Platform.runLater(() -> {
                                                        progressController.setSubtaskProgressBar(
                                                                (double) frame * frameInNodeFinal);
                                                        progressController.setTaskProgressBar(
                                                                ((double) currentNodeNumberFinal * nodeInScriptFinal) +
                                                                        ((double) frame * frameInScriptFinal));
                                                        progressController.setOverallProgressBar(
                                                                ((double) script * scriptInAll) +
                                                                        ((double) currentNodeNumberFinal * nodeInAllFinal) +
                                                                        ((double) frame * frameInAllFinal));
                                                        progressController.setSubtaskProgressLabel(
                                                                frame + "/" + frameCount);
                                                    });
                                                    if (renderFeedback.getInt("frame") == currentScriptLastFrame) {
                                                        final int newNodeNumber = ++currentNodeNumber;
                                                        final int nodeCount = currentScriptNodeCount;
                                                        Platform.runLater(() -> {
                                                            progressController.setSubtaskNameLabel("Done");
                                                            progressController.setTaskProgressBar(
                                                                    (double) newNodeNumber * nodeInScriptFinal);
                                                            progressController.setTaskProgressLabel(
                                                                    newNodeNumber + "/" + nodeCount);
                                                        });
                                                        rendering = false;
                                                    }
                                                } else {
                                                    Platform.runLater(() -> {
                                                        progressController.setSubtaskNameLabel("Wrong render order");
                                                    });
                                                    return Boolean.FALSE;
                                                }
                                                break;
                                        }
                                    }
                                    break;
                                case "doneProcessing":
                                    processing = false;
                                    Platform.runLater(() -> {
                                        progressController.setOverallProcessLabel("Done");
                                    });
                                    break;
                                case "canceled":
                                    processing = false;
                                    Platform.runLater(() -> {
                                        progressController.setOverallProcessLabel("Canceled");
                                    });
                                    break;
                                default:
                                    Platform.runLater(() -> {
                                        progressController.setOverallProcessLabel("Error communicating with slave node");
                                    });
                                    processing = false;
                                    break;
                            } // switch (slaveFeedback.getString("mode"))
                        } else {
                            Platform.runLater(() -> {
                                progressController.setOverallProcessLabel("Error communicating with slave node");
                            });
                            System.out.println("Error communicating with slave node");
                            return Boolean.FALSE;
                        }
                    } // while (processing)

                    ++currentScriptNumber;
                    final String overallProgressLabel = currentScriptNumber + "/" + scriptCount;
                    final double overallProgressBar = ((double) currentScriptNumber * scriptInAll);
                    Platform.runLater(() -> {
                        progressController.setTaskNameLabel("Done");
                        progressController.setOverallProgressLabel(overallProgressLabel);
                        progressController.setOverallProgressBar(overallProgressBar);
                    });
                    currentNodeNumber = 0;
                } // for (ScriptEntry scriptEntry : scriptRenderList)
                return Boolean.TRUE;
            }
        }; //final Task Boolean renderTask = new Task<Boolean>()

        final Thread thread = new Thread(renderTask);
        thread.setDaemon(true);
        thread.start();
    }

    private boolean setUpServer() {
        try {
            if (server == null)
                server = new ServerSocket(0, 10, InetAddress.getByName("localhost"));
            if (server.isClosed())
                server = new ServerSocket(0, 10, InetAddress.getByName("localhost"));
        } catch (IOException exception) {
            System.out.println("Couldn't open server socket");
            return false;
        }
        return true;
    }

    private boolean setUpSlave() {
        if (slaveNodeReady) {
            return true;
        } else {
            try {
                StringBuilder cmdBuilder = new StringBuilder()
                        .append("start \"Nuke Render Manager - Slave Node\" ")
                        .append(nukeExePath)
                        .append(" -t ")
                        .append(slaveNodePath)
                        .append(" --port=")
                        .append(server.getLocalPort());
                slaveNodeProcess = Runtime.getRuntime().exec("cmd.exe /c \"" + cmdBuilder.toString() + "\"");

            } catch (IOException exception) {
                System.out.println("Couldn't start the slave node process");
                exception.printStackTrace();
                return false;
            }
            try {
                Socket slaveNodeTcpSocket = server.accept();
                slaveNodeSocket = new TcpMessageSocket(slaveNodeTcpSocket);
                JSONObject readyMessage = slaveNodeSocket.readJson();
                if (!readyMessage.getString("mode").equals("slaveReady")) {
                    System.out.println("Couldn't connect to slave node");
                    slaveNodeProcess.destroy();
                    return false;
                }
            } catch (IOException | JSONException exception) {
                System.out.println("Couldn't connect to slave node");
                slaveNodeProcess.destroy();
                return false;
            }
            slaveNodeReady = true;
            return true;
        }
    }

    private JSONObject makeRenderJson(String script, boolean renderOnlyNodesNamed, String[] writeNodeNames) {
        JSONObject json = new JSONObject();
        json.put("mode", "render");
        json.put("script", script);
        json.put("renderOnlyNodesNamed", renderOnlyNodesNamed);
        if (renderOnlyNodesNamed) {
            JSONArray writeNodeNamesArray = new JSONArray();
            for (String writeNodeName : writeNodeNames)
                writeNodeNamesArray.put(writeNodeName);
            json.put("writeNodeNames", writeNodeNamesArray);
        } else {
            json.put("writeNodeNames", new JSONArray());
        }
        return json;
    }

    /**
     * Returns the script render queue
     *
     * @return the script render list
     * @since 0.0.1
     */
    ObservableList<ScriptEntry> getScriptRenderList() {
        return scriptRenderList;
    }

    /**
     * Returns the primary JavaFX {@code Stage}
     * Used to parent dialogs from the controller
     *
     * @return the primary stage
     * @since 0.0.1
     */
    Stage getPrimaryStage() {
        return primaryStage;
    }

}
