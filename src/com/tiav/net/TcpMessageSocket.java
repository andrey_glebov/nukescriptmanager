/*
 * Copyright (c) 2015. Andrey Glebov <andrey458641387@gmail.com>.
 * All rights reserved.
 */

package com.tiav.net;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * @author Andrey Glebov
 * @version 1.0
 */
public class TcpMessageSocket {

    private Socket socket = null;

    private DataInputStream inputStream = null;

    private DataOutputStream outputStream = null;

    private Charset charset = Charset.forName("UTF-8");

    public TcpMessageSocket(Socket socket) throws IOException {
        this.socket = socket;
        inputStream = new DataInputStream(socket.getInputStream());
        outputStream = new DataOutputStream(socket.getOutputStream());
    }

    public byte[] readMessage() throws IOException {
        while (true)
            if (inputStream.available() >= 4)
                break;
        int msgSize = inputStream.readInt();
        while (true)
            if (inputStream.available() >= msgSize)
                break;
        byte[] data = new byte[msgSize];
        if (inputStream.read(data) == msgSize)
            return data;
        else
            throw new IOException("Socket failed to get message", null);
    }

    public String readString() throws IOException {
        while (true)
            if (inputStream.available() >= 4)
                break;
        int msgSize = inputStream.readInt();
        while (true)
            if (inputStream.available() >= msgSize)
                break;
        byte[] data = new byte[msgSize];
        if (inputStream.read(data) == msgSize)
            return new String(data, charset);
        else
            throw new IOException("Socket failed to get message", null);
    }

    public JSONObject readJson() throws IOException, JSONException {
        while (true)
            if (inputStream.available() >= 4)
                break;
        int msgSize = inputStream.readInt();
        while (true)
            if (inputStream.available() >= msgSize)
                break;
        byte[] data = new byte[msgSize];
        if (inputStream.read(data) == msgSize)
            return new JSONObject(new String(data, charset));
        else
            throw new IOException("Socket failed to get message", null);
    }

    public byte readByte() throws IOException {
        while (true)
            if (inputStream.available() >= 1)
                break;
        return inputStream.readByte();
    }

    public byte[] readBytes(int size) throws IOException {
        while (true)
            if (inputStream.available() >= size)
                break;
        byte[] data = new byte[size];
        inputStream.read(data);
        return data;
    }

    public int readInt() throws IOException {
        while (true)
            if (inputStream.available() >= 4)
                break;
        return inputStream.readInt();
    }

    public void writeData(byte[] data) throws IOException {
        outputStream.writeInt(data.length);
        outputStream.write(data);
    }

    public void writeString(String string) throws IOException {
        byte[] data = string.getBytes(charset);
        outputStream.writeInt(data.length);
        outputStream.write(data);
    }

    public void writeJson(JSONObject json) throws IOException {
        byte[] data = json.toString().getBytes(charset);
        outputStream.writeInt(data.length);
        outputStream.write(data);
    }

    public void close() throws IOException {
        try {
            outputStream.flush();
            inputStream.close();
            outputStream.close();
            socket.shutdownInput();
            socket.shutdownOutput();
        } finally {
            socket.close();
        }
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public Charset getCharset() {
        return charset;
    }

}
