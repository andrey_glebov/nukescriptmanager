__author__ = "Andrey Glebov"
__copyright__ = "Copyright (c) 2015 Andrey Glebov"
__email__ = "andrey458641387@gmail.com"

__version__ = "0.0.2"
__status__ = "Development"

import argparse
import json
import socket
import struct
import sys
import threading

import nuke


def send_json(sock, json_object):
    raw_message = json.dumps(json_object)
    raw_message_size = struct.pack('>i', len(raw_message))
    sock.send(raw_message_size)
    sock.send(raw_message)


def receive_json(sock):
    total_len = 0
    total_data = []
    size = sys.maxint
    size_data = sock_data = ''
    receive_size = 8192
    while total_len < size:
        sock_data = sock.recv(receive_size)
        if not total_data:
            if len(sock_data) > 4:
                size_data += sock_data
                size = struct.unpack('>i', size_data[:4])[0]
                receive_size = size
                if receive_size > 524288:
                    receive_size = 524288
                total_data.append(size_data[4:])
            else:
                size_data += sock_data
        else:
            total_data.append(sock_data)
        total_len = sum([len(i) for i in total_data])
    raw_message = ''.join(total_data)
    return json.loads(raw_message)


class CancelHandler(threading.Thread):
    cancel_set = threading.Event()
    run_set = threading.Event()

    def __init__(self, sock):
        threading.Thread.__init__(self)
        self._socket = sock

    def run(self):
        while True:
            if self.run_set.is_set():
                msg = receive_json(self._socket)
                if 'mode' in msg and msg['mode'] == 'cancel':
                    self.run_set.clear()
                    self.cancel_set.set()
                    print 'Canceled'
                    print json.dumps(msg)


class NukeSlaveNode:
    _socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    _cancel_handler = CancelHandler(_socket)

    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--port', type=int, required=True)
        port = parser.parse_args().port

        self._setup_nuke()
        self._socket.connect(('localhost', port))
        send_json(self._socket, {'mode': 'slaveReady'})
        self._slave_loop()

    def _slave_loop(self):
        self._cancel_handler.start()

        while True:
            task = receive_json(self._socket)
            print json.dumps(task)

            # self._cancel_handler.run_set.set()
            if 'mode' in task:
                if task['mode'] == 'render':
                    if not self._cancel_handler.cancel_set.is_set():
                        self._render(task)
                elif task['mode'] == 'create':
                    self._create(task)
                elif task['mode'] == 'createAndRender':
                    self._create_and_render(task)
                elif task['mode'] == 'quit':
                    break
            else:
                print 'Invalid message from host'

            self._cancel_handler.run_set.clear()
            if self._cancel_handler.cancel_set.is_set():
                send_json(self._socket, {'mode': 'canceled'})
            else:
                send_json(self._socket, {'mode': 'doneProcessing'})
            self._cancel_handler.cancel_set.clear()

    def _render(self, task):
        nuke.scriptOpen(task['script'])

        if self._cancel_handler.cancel_set.is_set():
            return

        first_frame = int(nuke.Root().knob('first_frame').value())
        last_frame = int(nuke.Root().knob('last_frame').value())
        nodes_to_render = []

        for node in nuke.allNodes('Write', nuke.root()):
            render = True
            if task['renderOnlyNodesNamed']:
                render = False
                for write_node_name in task['writeNodeNames']:
                    if write_node_name == node['name'].value():
                        render = True
                        break
            if render:
                nodes_to_render.append(node)

        script_render_data = {
            'mode': 'scriptRenderData',
            'script': task['script'],
            'nodeCount': len(nodes_to_render),
            'firstFrame': first_frame,
            'lastFrame': last_frame
        }

        if self._cancel_handler.cancel_set.is_set():
            return

        send_json(self._socket, script_render_data)

        for node in nodes_to_render:
            if self._cancel_handler.cancel_set.is_set():
                break
            node_render_data = {
                'mode': 'nodeRenderData',
                'name': node['name'].value(),
                'file': node['file'].value()
            }
            send_json(self._socket, node_render_data)
            if self._cancel_handler.cancel_set.is_set():
                break
            nuke.execute(node, first_frame, last_frame)

        nuke.scriptClose()

    def _create(self, task):
        print 'create'

    def _create_and_render(self, task):
        print 'createAndRender'

    def _setup_nuke(self):
        nuke.addBeforeFrameRender(lambda: send_json(self._socket, {'mode': 'frameRenderBegin', 'frame': nuke.frame()}))
        nuke.addAfterFrameRender(lambda: send_json(self._socket, {'mode': 'frameRenderEnd', 'frame': nuke.frame()}))

    def _post_frame_render(self):
        if nuke.frame() == int(nuke.Root().knob('last_frame').value()):
            print 'Stopped cancel handler'
            self._cancel_handler.run_set.clear()
        send_json(self._socket, {'mode': 'frameRenderEnd', 'frame': nuke.frame()})


slaveNode = NukeSlaveNode()